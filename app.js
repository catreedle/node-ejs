const express = require('express')
const app = express()
const fs = require('fs')
const PORT = 3000
var path = require ('path')
var user = require('./user.json')
var bodyParser = require('body-parser')
var uuid = require('uuid')
var multer  = require('multer')

app.set('view engine', 'ejs');
// using app.use to serve up static CSS files in public/assets/ folder when /public link is called in ejs files
// app.use("/route", express.static("foldername"));
app.use('/public', express.static('public'));

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/images')
  },
  filename: function (req, file, cb) {
    cb(null, uuid() + path.extname(file.originalname))
  }
})
 
var upload = multer({ storage: storage })

app.use(express.static('public'))

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())
var middleWareLog = (req, res, next) => {
  console.log(req.method+ "")
}
app.get('/', function (req, res) {
  res.render('index', {users: user})
})

app.get('/user', function (req, res, next) {
  res.send(user)
})

app.get('/user/:id', function (req, res, next) {
  var findUser = user.find(found => found.id === req.params.id)
  if (findUser) {
    res.render('ViewUser', {user: findUser})
  } else {
    res.json({
      success: false,
      message: "user not found"
    })
  }
})

app.post('/user', upload.single('avatar'), (req, res, next) => {
  var newUser = {
    user: req.body.user,
    id: uuid(),
    hp: req.body.hp,
    email: req.body.email,
    image: req.file ? "/images/"+req.file.filename : "https://ui-avatars.com/api/?name="+req.body.user
  }
  user = [...user, newUser]
  res.json({
    success: true,
    message: "user created",
    data: newUser
  })
  fs.writeFile('user.json', JSON.stringify(user), (err) => {
    if (err) {
      res.json({
        success: false,
        message: "fail to create user"
      })
    }
  })
})

app.delete('/user/:id', (req, res, next) => {
  var findUser = user.findIndex(found => found.id === req.params.id)
  user.splice(findUser, 1)

  if (findUser > -1) {
    fs.writeFile('user.json', JSON.stringify(user), (err) => {
      if (err) {
        res.json({
          success: false,
          message: "fail to create"
        })
      }
    })
    res.json({
      success: true,
      message: "user deleted",
      data: user
    })
  } else {
    res.json({
      success: false,
      message: "user not found"
    })
  }
})

app.put('/user/:id', (req, res, next) => {
  var findUser = user.findIndex(found => found.id === req.params.id)

  if (findUser > -1) {
    // var update = { id: user[findUser].id, user: req.body.user }
    // user.splice(findUser, 1, update)

    //tanpa method splice
    user[findUser] = {
      user: req.body.user || user[findUser].user,
      hp: req.body.hp,
      email: req.body.email,
      id: user[findUser].id
    }

    fs.writeFile('user.json', JSON.stringify(user), (err) => {
      if (err) {
        res.json({
          success: false,
          message: "fail to update user"
        })
      }
    })
    res.json({
      success: true,
      message: "user updated",
      data: user[findUser]
    })
  } else {
    res.json({
      success: false,
      message: "user not found"
    })
  }
})

app.patch('/user/:id', (req, res, next) => {
  var findUser = user.findIndex(found => found.id === req.params.id)

  if (findUser > -1) {
    // var update = { id: user[findUser].id, user: req.body.user }
    // user.splice(findUser, 1, update)

    //tanpa method splice
    user[findUser] = {
      user: req.body.user ,
      id: user[findUser].id,
      hp: req.body.hp || user[findUser].hp,
      email: req.body.email || user[findUser].email,
      image: req.file || user[findUser].image
    }

    fs.writeFile('user.json', JSON.stringify(user), (err) => {
      if (err) {
        res.json({
          success: false,
          message: "fail to update user"
        })
      }
    })
    res.json({
      success: true,
      message: "user updated",
      data: user[findUser]
    })
  } else {
    res.json({
      success: false,
      message: "user not found"
    })
  }
})

app.listen(PORT, () => {
  console.log("Server starts on port " + PORT)
})